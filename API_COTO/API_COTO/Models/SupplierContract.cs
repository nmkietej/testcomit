﻿using System;
using System.Collections.Generic;

namespace API_COTO.Models
{
    public partial class SupplierContract
    {
        public int Id { get; set; }
        public string ContractNo { get; set; }
        public int? SupplierId { get; set; }
        public string SupplierName { get; set; }
        public int? ContractType { get; set; }
        public DateTimeOffset? ContractDate { get; set; }
        public bool? IsFullShipment { get; set; }
        public string SupplierPo { get; set; }
        public DateTimeOffset? SupplierDate { get; set; }
        public DateTimeOffset? ExpectedEtd { get; set; }
        public int? StatusId { get; set; }
        public DateTimeOffset? ValidFromDate { get; set; }
        public DateTimeOffset? ValidToDate { get; set; }
        public int? ResponseUserId { get; set; }
        public bool? IsActive { get; set; }
        public bool? IsDelete { get; set; }
        public DateTimeOffset? CreatedDate { get; set; }
        public int? CreatedBy { get; set; }
        public DateTimeOffset? ModifyDate { get; set; }
        public int? ModifyBy { get; set; }
        public string SearchString { get; set; }
        public int? SortOrder { get; set; }
        public string Note { get; set; }
        public string PaymentTerm { get; set; }
        public string ExpectedNote { get; set; }
    }
}
