﻿using System;
using System.Collections.Generic;

namespace API_COTO.Models
{
    public partial class SupplierContractShipmentActual
    {
        public int Id { get; set; }
        public int? SupplierContractId { get; set; }
        public string SupplierOrderId { get; set; }
        public int? SupplierOrderInternalId { get; set; }
        public string WaybillCode { get; set; }
        public int? HarborTo { get; set; }
        public string InvoiceNumber { get; set; }
        public DateTimeOffset? InvoiceDate { get; set; }
        public string Currency { get; set; }
        public int? ExchangeRateId { get; set; }
        public double? InsFeePort { get; set; }
        public double? InsFeeDoor { get; set; }
        public int? StoreToId { get; set; }
        public string StoreTo { get; set; }
        public DateTimeOffset? RequestedEtd { get; set; }
        public DateTimeOffset? RequestedEta { get; set; }
        public DateTimeOffset? BookedEtd { get; set; }
        public DateTimeOffset? BookedEta { get; set; }
        public bool IsSend { get; set; }
        public int? JointShipmentId { get; set; }
        public bool? IsActive { get; set; }
        public bool? IsDelete { get; set; }
        public DateTimeOffset? CreatedDate { get; set; }
        public int? CreatedBy { get; set; }
        public DateTimeOffset? ModifyDate { get; set; }
        public int? ModifyBy { get; set; }
        public DateTimeOffset? LoadingDate { get; set; }
        public string Note { get; set; }
        public int? InsurranceTypeId { get; set; }
        public decimal? BaseFee { get; set; }
        public decimal? InsurranceFee { get; set; }
        public string Sosupplier { get; set; }
        public string SearchString { get; set; }
        public bool? IsFullDocument { get; set; }
        public bool? IsDelay { get; set; }
        public bool? IsCancel { get; set; }
        public int? CustomsDeclarationId { get; set; }
        public int? ShipmentRootId { get; set; }
    }
}
