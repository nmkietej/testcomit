﻿using System;
using System.Collections.Generic;

namespace API_COTO.Models
{
    public partial class TonKho0403
    {
        public string MaHang { get; set; }
        public string TenHang { get; set; }
        public string SoLo { get; set; }
        public DateTime? NgaySanXuat { get; set; }
        public DateTime? NgayHetHan { get; set; }
        public string Dvt { get; set; }
        public double? DauKy { get; set; }
        public double? NhapKho { get; set; }
        public double? XuatKho { get; set; }
        public double? CuoiKy { get; set; }
        public int? SoNgay { get; set; }
        public double? SoNam { get; set; }
        public int? SoThang { get; set; }
        public int? Store { get; set; }
    }
}
