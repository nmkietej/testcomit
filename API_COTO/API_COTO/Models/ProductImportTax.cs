﻿using System;
using System.Collections.Generic;

namespace API_COTO.Models
{
    public partial class ProductImportTax
    {
        public int Id { get; set; }
        public int ProductId { get; set; }
        public decimal? ImportTax { get; set; }
        public string ImportCode { get; set; }
        public DateTimeOffset? EffectiveDate { get; set; }
        public bool? IsDefault { get; set; }
        public bool? IsActive { get; set; }
        public string SearchString { get; set; }
        public bool? IsDeleted { get; set; }
        public int? SortOrder { get; set; }
        public int? CreatedBy { get; set; }
        public DateTimeOffset? CreatedDate { get; set; }
        public int? ModifiedBy { get; set; }
        public DateTimeOffset? ModifiedDate { get; set; }
    }
}
