﻿using System;
using System.Collections.Generic;

namespace API_COTO.Models
{
    public partial class SupplierContractShipmentActualDetail
    {
        public int Id { get; set; }
        public int? SupplierContractShipmentActualId { get; set; }
        public int? SupplierContractId { get; set; }
        public int? ProductId { get; set; }
        public string ProductName { get; set; }
        public double? Quantity { get; set; }
        public int? QuantityConfirm { get; set; }
        public double? Price { get; set; }
        public string MadeIn { get; set; }
        public int? PackageUnitId { get; set; }
        public DateTimeOffset? ProductionDate { get; set; }
        public decimal? ExpiredDate { get; set; }
        public double? PackageAmount { get; set; }
        public double? MassPerPackage { get; set; }
        public string LotNumber { get; set; }
        public bool? IsActive { get; set; }
        public bool? IsDelete { get; set; }
        public DateTimeOffset? CreatedDate { get; set; }
        public int? CreatedBy { get; set; }
        public DateTimeOffset? ModifyDate { get; set; }
        public int? ModifyBy { get; set; }
        public bool? IsJoin { get; set; }
        public string BatchNote { get; set; }
        public int? PackageUnitPalletId { get; set; }
        public int? SortOrder { get; set; }
        public string ImportTaxCode { get; set; }
        public decimal? ImportTax { get; set; }
        public decimal? Vat { get; set; }
    }
}
