﻿using System;
using System.Collections.Generic;

namespace API_COTO.Models
{
    public partial class SupplierContractLog
    {
        public string Id { get; set; }
        public int? SupplierContract { get; set; }
        public string Content { get; set; }
        public DateTimeOffset CreatedDate { get; set; }
        public int CreatedBy { get; set; }
    }
}
