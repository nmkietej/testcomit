﻿using System;
using System.Collections.Generic;

namespace API_COTO.Models
{
    public partial class SupplierContractShipmentPlanned
    {
        public int Id { get; set; }
        public int? SupplierContractId { get; set; }
        public string ShipmentName { get; set; }
        public bool? IsActive { get; set; }
        public bool? IsDelete { get; set; }
        public DateTimeOffset? CreatedDate { get; set; }
        public int? CreatedBy { get; set; }
        public DateTimeOffset? ModifyDate { get; set; }
        public int? ModifyBy { get; set; }
        public int? SortOrder { get; set; }
    }
}
