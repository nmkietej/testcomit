﻿using System;
using System.Collections.Generic;

namespace API_COTO.Models
{
    public partial class ProductBk1703
    {
        public int ProductId { get; set; }
        public string ProductCode { get; set; }
        public string ProductName { get; set; }
        public string CorrectName { get; set; }
        public int? ProductTypeId { get; set; }
        public int? SupplierId { get; set; }
        public string OriginId { get; set; }
        public int? IndustryId { get; set; }
        public int? LeadTime { get; set; }
        public int? PackageUnitId { get; set; }
        public string Remark { get; set; }
        public bool? IsActive { get; set; }
        public string SearchString { get; set; }
        public bool? IsDeleted { get; set; }
        public int? CreatedBy { get; set; }
        public DateTimeOffset? CreatedDate { get; set; }
        public int? ModifiedBy { get; set; }
        public DateTimeOffset? ModifiedDate { get; set; }
        public int? SortOrder { get; set; }
        public int? CurrentStatusId { get; set; }
        public string TaxCode { get; set; }
        public int? UnitExchangeId { get; set; }
        public string ProductCommerceName { get; set; }
        public bool? IsConfirm { get; set; }
        public string Note { get; set; }
    }
}
